<%@page import="java.io.*, java.awt.Color,java.text.*,java.util.*,com.lowagie.text.*, com.lowagie.text.pdf.*, atae.thin.pres.AtaeSvPresentacionUtils;"%><%! 
String formateaAMoneda(String cadena){ 
	double value = 0;
	String str_izq = "";
	String str_der = "";
	String salida = "";
	String patron = "$###,###.##";
	if(cadena!=null && !cadena.equals("")) {
		cadena = cadena.replaceAll(",", "");
		cadena = cadena.trim();
		if(cadena.equals("0") || cadena.equals("0.0")) return "0.00";
		str_izq = cadena.substring(0,cadena.length() - 2);
		str_der = cadena.substring(cadena.length() - 2,cadena.length());
		value = Double.parseDouble(str_izq);
		DecimalFormat myFormatter = new DecimalFormat(patron);
		salida = myFormatter.format(value);
		salida = salida.replace('.',',');
		salida = salida + "."+str_der;
	}
	return salida;
};
String fecha(){
	 String sFormato="dd/MM/yyyy";
	 SimpleDateFormat formato = new SimpleDateFormat(sFormato, new Locale("es", "MX"));
     String fecha_actual = formato.format(new Date());
	 return fecha_actual;
} 

String formateaFecha(String fecha){
	try{
		if(fecha!=null && !fecha.equals("")){
			String ar[]=fecha.split("-");
			return ar[0]+"/"+ar[1]+"/"+ar[2];
		}
	}catch(Exception e){return "";}
	
	return "";
}
String enmascaraCuenta(String cadena) {
	try{
		cadena = cadena.substring(10);
		cadena="**********"+cadena;	
		return cadena;	
	}
	catch(Exception e){return cadena;}
}
String formateaAEntero(String cad){
	String plazo2="";
	//try{
		if(!cad.equals("")){
			int plz = Integer.parseInt(cad,10);
			plazo2 = plz+" ";
		}
	//}catch(Exception e){ return "NADA";}
	
	return plazo2;
};
%><%
atae.thin.pres.AtaeSvPresentacionUtils utils = atae.thin.pres.AtaeSvPresentacionUtils.getInstance(request, getServletConfig().getServletContext());
%><% 

System.out.println("BODY");


response.setContentType("application/pdf");
response.setHeader("title","ComprobanteRecalendarizacion.pdf");
response.setHeader("Content-Disposition", "attachment; filename=Comprobante_Pago_Unidades.pdf") ;

//Creando el documento PDF:
Document document = new Document();
Font f;
PdfTemplate tp;
Phrase pt;
Color colorF;

String sobtalc = "";
float fsobtalc = 0;
try {
sobtalc = (utils.getValorContexto("SOBTALC")!=null)?utils.getValorContexto("SOBTALC").trim():"";
fsobtalc = Float.parseFloat(sobtalc);
DecimalFormatSymbols punto = new DecimalFormatSymbols();
punto.setDecimalSeparator('.');
DecimalFormat formateador = new DecimalFormat("##.00", punto);
sobtalc = formateador.format(fsobtalc);
}
catch(Exception e) {
	sobtalc = "0.00";
}

String fechaPago=fecha();
/*String cuentaCargo=enmascaraCuenta(utils.getValorContexto("REG_UG02.CTACHEQ")!=null?utils.getValorContexto("REG_UG02.CTACHEQ").trim():"");
String importeTotal=formateaAMoneda(utils.getValorContexto("U501.TOTMN")!=null?utils.getValorContexto("U501.TOTMN").trim():"");
String folio=utils.getValorContexto("REG_UG09.DCVEUNI")!=null?utils.getValorContexto("REG_UG09.DCVEUNI").trim():"";
String convenio=utils.getValorContexto("REG_UG02.CONVENIO")!=null?utils.getValorContexto("REG_UG02.CONVENIO").trim():"";
String marca = utils.getValorContexto("REG_UG02.DESMAR")!=null?utils.getValorContexto("REG_UG02.DESMAR").trim():"";
String modelo=utils.getValorContexto("REG_UG09.DMODELO")!=null?utils.getValorContexto("REG_UG09.DMODELO").trim():"";
String vin=utils.getValorContexto("REG_UG09.DNUMVIN")!=null?utils.getValorContexto("REG_UG09.DNUMVIN").trim():"";
String disp=utils.getValorContexto("REG_UG09.DNUMDIS")!=null?utils.getValorContexto("REG_UG09.DNUMDIS").trim():"";
String importe=formateaAMoneda(utils.getValorContexto("REG_UG09.IMPORTE")!=null?utils.getValorContexto("REG_UG09.IMPORTE").trim():"");
String color=utils.getValorContexto("REG_UG09.DCOLOR")!=null?utils.getValorContexto("REG_UG09.DCOLOR").trim():"";
String version =utils.getValorContexto("REG_UG09.VERSION")!=null?utils.getValorContexto("REG_UG09.VERSION").trim():"";
String anio =utils.getValorContexto("REG_UG09.DANIO")!=null?utils.getValorContexto("REG_UG09.DANIO").trim():"";
String interes=utils.getValorContexto("REG_UG09.INTERES")!=null?formateaAMoneda(utils.getValorContexto("REG_UG09.INTERES").trim()):"";
String fechaVenci=utils.getValorContexto("REG_UG09.FEVENCI")!=null?utils.getValorContexto("REG_UG09.FEVENCI").trim():"";
String interesesPagados ="";*/
String cuentaCargo="415231000000000000";
String importeTotal="1,000";
String folio="123456789";
String convenio="0025";
String marca = "MARCA";
String modelo="MODELO";
String vin="VIN";
String disp="DISPONIBLE";
String importe="2,356.56";
String color="BLUE";
String version ="1.0";
String anio ="2013";
String interes="3%";
String fechaVenci="11/08/2013";
String interesesPagados ="1%";
String tazaInteresAnual="TIIE + "+sobtalc+"%";
float intPag = 0;

try{

	System.out.println("EN EL TRY");

	ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	PdfWriter writer = PdfWriter.getInstance(document, buffer);
	document.open();
	PdfContentByte cb = writer.getDirectContent();
	
	InputStream pdf;
	PdfReader pdfReader;
	int totalPages = 0;
	PdfImportedPage pagina;
	document.setPageSize(PageSize.LETTER);
	
	pdf = new FileInputStream(application.getRealPath("/lbmx_es_web_pub/Plantilla_Pago_Unidades.pdf"));
	//pdf = new FileInputStream("/qamxexhae/ppmx/online/es/web/pub/Plantilla_Pago_Unidades.pdf");
	pdfReader = new PdfReader(pdf);
	colorF = new Color(9,79,164);
	f = new Font(Font.DEFAULTSIZE, 8, Font.NORMAL, colorF);
	document.newPage();
	pagina = writer.getImportedPage(pdfReader, 1);
	cb.addTemplate(pagina, 0, 0);
	pdfReader.close();	 
	
	//Convenio
	tp = cb.createTemplate(175, 50);
    pt = new Phrase(convenio, f);
    ColumnText.showTextAligned(tp, Element.ALIGN_LEFT, pt, 0, 1 , 0);
    cb.addTemplate(tp, 309, 669);
	
	//Marca
	tp = cb.createTemplate(175, 50);
    pt = new Phrase(marca, f);
    ColumnText.showTextAligned(tp, Element.ALIGN_LEFT, pt, 0, 1 , 0);
    cb.addTemplate(tp, 309, 654);
	
	//Cuenta de Cargo
	tp = cb.createTemplate(175, 50);
    pt = new Phrase(cuentaCargo, f);
    ColumnText.showTextAligned(tp, Element.ALIGN_LEFT, pt, 0, 1 , 0);
    cb.addTemplate(tp, 309, 638);
	
	//Tasa de Interes Anual
	tp = cb.createTemplate(175, 50);
    pt = new Phrase(tazaInteresAnual, f);
    ColumnText.showTextAligned(tp, Element.ALIGN_LEFT, pt, 0, 1 , 0);
    cb.addTemplate(tp, 309, 625);
	
	//No de Dispocisión
	tp = cb.createTemplate(175, 50);
    pt = new Phrase(disp, f);
    ColumnText.showTextAligned(tp, Element.ALIGN_LEFT, pt, 0, 1 , 0);
    cb.addTemplate(tp, 309, 590);
		
	//Fecha de pago
	tp = cb.createTemplate(175, 50);
    pt = new Phrase(fechaPago, f);
    ColumnText.showTextAligned(tp, Element.ALIGN_LEFT, pt, 0, 1 , 0);
    cb.addTemplate(tp, 309, 577);
	
	//Importe
	tp = cb.createTemplate(175, 50);
    pt = new Phrase(importe, f);
    ColumnText.showTextAligned(tp, Element.ALIGN_LEFT, pt, 0, 1 , 0);
    cb.addTemplate(tp, 309, 564);
	
	//Interes
	tp = cb.createTemplate(175, 50);
    pt = new Phrase(interes, f);
    ColumnText.showTextAligned(tp, Element.ALIGN_LEFT, pt, 0, 1 , 0);
    cb.addTemplate(tp, 309, 550);
					
	//Importe Total
	tp = cb.createTemplate(175, 50);
    pt = new Phrase(importeTotal, f);
    ColumnText.showTextAligned(tp, Element.ALIGN_LEFT, pt, 0, 1 , 0);
    cb.addTemplate(tp, 309, 538);
	
	//Folio
	tp = cb.createTemplate(175, 50);
    pt = new Phrase(folio, f);
    ColumnText.showTextAligned(tp, Element.ALIGN_LEFT, pt, 0, 1 , 0);
    cb.addTemplate(tp, 309, 503);
		
	//Modelo
	tp = cb.createTemplate(175, 50);
    pt = new Phrase(modelo, f);
    ColumnText.showTextAligned(tp, Element.ALIGN_LEFT, pt, 0, 1 , 0);
    cb.addTemplate(tp, 309, 489);
	
	//VIN
	tp = cb.createTemplate(175, 50);
    pt = new Phrase(vin, f);
    ColumnText.showTextAligned(tp, Element.ALIGN_LEFT, pt, 0, 1 , 0);
    cb.addTemplate(tp, 309, 476);
	
	//Color
	tp = cb.createTemplate(175, 50);
    pt = new Phrase(color, f);
    ColumnText.showTextAligned(tp, Element.ALIGN_LEFT, pt, 0, 1 , 0);
    cb.addTemplate(tp, 309, 461);
	
	//Version
	tp = cb.createTemplate(175, 50);
    pt = new Phrase(version, f);
    ColumnText.showTextAligned(tp, Element.ALIGN_LEFT, pt, 0, 1 , 0);
    cb.addTemplate(tp, 309, 447);
	
	//Anio
	tp = cb.createTemplate(175, 50);
    pt = new Phrase(anio, f);
    ColumnText.showTextAligned(tp, Element.ALIGN_LEFT, pt, 0, 1 , 0);
    cb.addTemplate(tp, 309, 434);
								
	document.close();
	
	DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
	byte[] bytes = buffer.toByteArray();
	response.setContentLength(bytes.length);
	
	for(int i = 0; i < bytes.length; i++) {
		dataOutput.writeByte(bytes[i]);
	}
	buffer.flush();
	buffer.close();
}catch(DocumentException e){
	e.printStackTrace();
}catch(IOException ioe) {
	ioe.printStackTrace();
}

%>
